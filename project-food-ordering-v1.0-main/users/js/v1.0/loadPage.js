import { gPizzaChosen } from "../v2.0/cartPage/storage.js";
import { changeCart } from "../v2.0/from v1.0 to v2.0/choosePizza.js";
import loadBlogs from "./blogs/loadBlogs.js";
import loadPizza from "./pizzas/loadPizzas.js";

const onLoadingPage = () => {
    loadPizza()
    loadBlogs()
    changeCart(gPizzaChosen)
}
export default onLoadingPage;