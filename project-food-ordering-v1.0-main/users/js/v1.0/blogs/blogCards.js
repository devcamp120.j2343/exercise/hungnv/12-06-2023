export const blogsCol3 = (paramBlog) => {
    return (`
        <div class="col-md-3" id="blog-${paramBlog.id}-parent">
            ${blogCol3Child(paramBlog)}
        </div>
    `)
}

export const blogCol6 = (paramBlog) => {//id=3
    return (`
        <div class="col-md-6">
            <div class="px-0 col-md-12 source-sans">
                <div>
                    <img src="${paramBlog.imageUrl}" class="col-md-12 px-0">
                </div>
                <div class="px-2 pb-3 bg-white">
                    <h5 class="pt-3">
                        ${paramBlog.title}
                    </h5>
                    <small>
                        ${paramBlog.description}
                    </small>
                </div>
            </div>
        </div>
    `)
}

export const blogCol3Child = (paramBlog) => {
    return (`
    <div class="px-0 col-md-12 source-sans">
                <div>
                    <img src="${paramBlog.imageUrl}" class="col-md-12 px-0">
                </div>
                <div class="px-2 pb-3 bg-white">
                    <h5 class="pt-3">
                        ${paramBlog.title}
                    </h5>
                    <small>
                        ${paramBlog.description}
                    </small>
                </div>
            </div>
    `)
} 