export const carouselBlogCards = (paramBlog) => {
    return (`
        <div id="carouselBlogIndicators" class="carousel slide col-md-12" style="height:max-content;" data-ride="carousel">
            
            <div class="carousel-inner" id="carousel-blog">
                ${oneSlideBlog(paramBlog)}
            </div>
            <a class="carousel-control-prev" href="#carouselBlogIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselBlogIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    `)

}

export const oneSlideBlog = (paramBlog) => {
    return (`
        <div class="carousel-item" id="slide-${paramBlog.id}-blog">
            <div class="px-0 col-md-12 source-sans">
                <div>
                    <img src="${paramBlog.imageUrl}" class="col-md-12 px-0">
                </div>
                <div class="px-2 pb-3 bg-white">
                    <h5 class="pt-3">
                        ${paramBlog.title}
                    </h5>
                    <small>
                        ${paramBlog.description}
                    </small>
                </div>
            </div>
        </div>
    `)

}