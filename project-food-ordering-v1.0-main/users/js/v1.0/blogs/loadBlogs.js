import { blogCol6, blogsCol3, blogCol3Child } from "./blogCards.js"
import { carouselBlogCards, oneSlideBlog } from "./carouselBlog.js"

const gBLOGS_URL = "https://64747ed27de100807b1b0fa4.mockapi.io/api/v1/blogs"

const loadBlogs = () => {
    $.ajax({
        url: gBLOGS_URL,
        type: 'GET',
        success: res => renderBlog(res),
        error: err => console.log(err)
    })
}
const renderBlog = (paramBlog) => {
    changeSlideBlog(paramBlog)
}

const changeSlideBlog = (paramBlog) => {
    var vWidth = window.innerWidth;
    paramBlog.map((blog, index) => {
        if (vWidth <= 768) {
            if (blog.id == '1') {
                $('#blog-cards').html('')
                $('#blog-cards').append(carouselBlogCards(blog))
            } else {
                $('#carousel-blog').append(oneSlideBlog(blog))
                $(`#slide-3-blog`).addClass('active')
            }
        } else {
            var vId = parseInt(blog.id)
            switch (index) {
                case 0:
                    $('#blog-cards').append(blogsCol3(blog))
                    break
                case 1:
                    $(`#blog-${vId - 1}-parent`).append(blogCol3Child(blog))
                    break
                case 2:
                    $('#blog-cards').append(blogCol6(blog))
                    break
                case 3:
                    $('#blog-cards').append(blogsCol3(blog))
                    break
                case 4:
                    $(`#blog-${vId - 1}-parent`).append(blogCol3Child(blog))
                    break
            }
        }
    })
}
export default loadBlogs