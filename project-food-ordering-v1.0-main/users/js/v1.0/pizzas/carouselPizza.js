export const carouselPizzaCards = (paramPizza) => {
    return (`
        <div id="carouselExampleIndicators" class="carousel slide col-md-12" style="height:max-content;" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active bg-warning" ></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1" class=" bg-warning"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2" class=" bg-warning"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="3" class=" bg-warning"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="4" class=" bg-warning"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="5" class=" bg-warning"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="6" class=" bg-warning"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="7" class=" bg-warning"></li>
            </ol>
            <div class="carousel-inner" id="carousel">
            ${oneSlidePizza(paramPizza)}
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    `)
}

export const oneSlidePizza = (paramPizza) => {
    return (`
        <div class="carousel-item" id="slide-${paramPizza.id}">
            <div class="pizza col-md-10 px-0 source-sans">
                <div>
                    <img class="d-block w-100" src="${paramPizza.imageUrl}" alt="First slide">
                </div>
                <div class="px-3 pb-3 bg-white" style="height:40%">
                    <div class="py-3 d-flex justify-content-between">
                        <p>${paramPizza.name}</p>
                        <span>$${paramPizza.price}</span>
                    </div>
                    <div class="d-flex justify-content-between">
                        <div>
                            <span class="custom-span-pizza-rate p-1 mr-1">
                                <img src="./imgs/start.png">
                                ${paramPizza.rating}
                            </span>
                            <span class="custom-span-pizza-rate p-1 ml-1">${paramPizza.time}</span>
                        </div>
                        <div><img src="./imgs/plus-btn.png"></div>
                    </div>
                </div>
            </div>
        </div>
    `)
}