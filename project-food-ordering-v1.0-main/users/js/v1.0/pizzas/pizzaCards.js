const pizzaCard = (paramPizza) => {
    return (`
        <div class="pizza px-0 source-sans">
            <div>
                <img src="${paramPizza.imageUrl}" class="col-md-12 px-0">
            </div>
            <div class="px-3 pb-3 bg-white" style="height:40%">
                <div class="py-3 d-flex justify-content-between">
                    <p>${paramPizza.name}</p>
                    <span>$${paramPizza.price}</span>
                </div>
                <div class="d-flex justify-content-between">
                    <div>
                        <span class="custom-span-pizza-rate p-1 mr-1">
                            <img src="./imgs/start.png">
                            ${paramPizza.rating}
                        </span>
                        <span class="custom-span-pizza-rate p-1 ml-1">${paramPizza.time}</span>
                    </div>
                    <div>
                    <i class="fa-solid fa-square-plus fa-2xl btn-plus-pizza" style="color: #f3ba00;" data-pizza='${JSON.stringify(paramPizza)}'></i>
                    </div>
                </div>
            </div>
        </div>
    `)
}
export default pizzaCard;

