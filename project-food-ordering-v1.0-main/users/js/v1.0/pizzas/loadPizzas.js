import pizzaCard from "./pizzaCards.js"
import { carouselPizzaCards, oneSlidePizza } from "./carouselPizza.js"
const gPIZZA_URL = "https://64747ed27de100807b1b0fa4.mockapi.io/api/v1/pizza"

const loadPizza = () => {
    $.ajax({
        url: gPIZZA_URL,
        type: 'GET',
        success: res => renderPizza(res),
        error: err => console.log(err)
    })
}
const renderPizza = (paramPizza) => {
    window.addEventListener("resize", changeSlide(paramPizza))
}

const changeSlide = (paramPizza) => {
    var vWidth = window.innerWidth;
    paramPizza.map(pizza => {
        if (vWidth <= 650) {
            if (pizza.id == '1') {
                $('#pizza-cards').html('')
                $('#pizza-cards').append(carouselPizzaCards(pizza))
            } else {
                $('#carousel').append(oneSlidePizza(pizza))
                $(`#slide-1`).addClass('active')
            }
        } else {
            $('#pizza-cards').append(pizzaCard(pizza))
        }
    })
}

export default loadPizza