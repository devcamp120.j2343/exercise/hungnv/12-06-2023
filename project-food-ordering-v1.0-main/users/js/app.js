import onLoadingPage from "./v1.0/loadPage.js"
import handleChoosePizza from "./v2.0/from v1.0 to v2.0/choosePizza.js"
import gotoCartPage, { gotoPizzasPage } from "./v2.0/from v1.0 to v2.0/cartHref.js"
import onLoadingCartPage from "./v2.0/loadPage.js"
import { checkCoupon, gCouponObj } from "./v2.0/cartPage/bill/checkCoupon.js"
import onBtnGoToPayment from "./v2.0/modal/payment/event.js"
import changeQuantity from "./v2.0/cartPage/add-subtract.js"
const app = () => {
    //hàm load trang chính
    onLoadingPage()

    //hàm load trang giỏ hàng
    onLoadingCartPage()

    //gán sự kiện ấn nút chọn pizza ở trang chính
    $('#pizza-cards').on('click', '.btn-plus-pizza', function () { handleChoosePizza(this) })

    //gán sự kiện ấn nút giỏ hàng ở trang chính => di chuyển sang trang giỏ hàng
    $('#icon-cart').on('click', gotoCartPage)

    $('#btn-load-more-pizzas').on('click', gotoPizzasPage)

    //gán sự kiện ấn nút thêm/bớt số lượng sản phẩm ở trang giỏ hàng
    $('#cart-tbody').on('click', '.btn-plus', function () { changeQuantity(this) })

    $('#cart-tbody').on('click', '.btn-subtract', function () { changeQuantity(this) })

    //gán sự kiện ấn nút redeem coupon ở trang giỏ hàng =>callAPI để tìm mã giảm giá phù hợp
    $('#btn-redeem').on('click', function () {
        checkCoupon()
        console.log(gCouponObj);
    })

    //gán sự kiện submit cho form Bill, mở modal payment
    $('#form-total').on('submit', function (event) {
        event.preventDefault()
        $('#create-payment-modal').modal('show')
    })
    //gán sự kiện ấn nút go to payment trên modal
    $('#btn-payment').on('click', function () {
        onBtnGoToPayment()
    })
    //gán sự kiện ấn nút complete trên modal thanh toán thành công
    $('#btn-sucess-payment').on('click', () => {
        window.location.href = 'index.html'
    })
}
app()