import { saveStorage } from "../cartPage/storage.js"
import { gPizzaChosen } from "../cartPage/storage.js"
//hàm xử lý khi ấn nút chọn thêm pizza => lưu vào localStorage => dùng ở app.js
const handleChoosePizza = (paramIcon) => {
    changeCart(gPizzaChosen)
    saveStorage(paramIcon)
    console.log(gPizzaChosen);
    handleUX(paramIcon)
}

const handleUX = (paramIcon) => {
    paramIcon.style.color = '#8a6c00'
}

//hàm đổi màu icon cart(header) và parse thông tin pizza thành mảng chứa các obj
export const changeCart = (paramChosen) => {
    if (paramChosen !== null) {
        $('#icon-cart').attr('src', './imgs/cart-yellow.png')
        $('#icon-cart-page').attr('src', './imgs/cart-yellow.png')
    } else {
        $('#icon-cart').attr('src', './imgs/cart-white.png')
        $('#icon-cart-page').attr('src', './imgs/cart-white.png')
    }
}
export default handleChoosePizza