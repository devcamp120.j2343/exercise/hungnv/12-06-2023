var gPizzaArr = []
var gPizzaChosenArr = []
export const gPizzaChosen = localStorage.getItem('pizza-chosen')

const saveStorage = (paramIcon) => {
    var vPizza = paramIcon.getAttribute('data-pizza')//lấy thông tin pizza được chọn từ dataset(dữ liệu dạng chuỗi)
    gPizzaChosenArr.push(vPizza)
    console.log(gPizzaChosenArr);
    localStorage.setItem('pizza-chosen', [ , ...getStorage().map(pizza => JSON.stringify(pizza))].join('-&-'))//lưu vào storage
}

const getStorage = () => {
    if (gPizzaChosen !== null) {
        if (!gPizzaChosen.includes('-&-')) {
            gPizzaArr = [JSON.parse(gPizzaChosen)]
        } else {
            var vPizzaChosenArr = gPizzaChosen.split('-&-')// mảng các chuỗi detail['']
            gPizzaArr = vPizzaChosenArr.map(pizza => JSON.parse(pizza))
        }
    } else {
        gPizzaArr = []
    }
    return gPizzaArr
}

//hàm xử lý storage => [[obj,số lần xuất hiện],...]
// lọc được các phần tử (obj detail) có id giống nhau thành 1 mảng gồm[obj,số lần xuất hiện]
//=>ra 1 mảng mới gồm các mảng phía trên
const handleStorage = (paramStorage) => {
    return paramStorage.reduce((acc, value) => {
        const { data, map } = acc;
        const ind = map.get(value.id);
        if (map.has(value.id)) {
            data[ind][1]++;//để tăng số đếm các phần tử giống nhau
        } else {
            map.set(value.id, data.push([value, 1]) - 1);//? chưa hiểu lắm
        }
        return { data, map };
    }, {
        data: [], //để push các obj và số lần xuất hiện tương ứng
        map: new Map()//Map là tập hợp các cặp key-val (viết dạng key=>value), tương tự obj nhưng có thể nhận key ở mọi kiểu dữ liệu
    }).data
};
//dùng id để set map và xử lý has,get, còn push vào data thì push cả obj
export { saveStorage, getStorage, handleStorage }