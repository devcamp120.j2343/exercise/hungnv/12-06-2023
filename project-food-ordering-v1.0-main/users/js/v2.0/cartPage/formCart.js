const formCart = (paramPizza, paramAmount) => {
    return (`
        <tr class='tr-cart-parent'>
            <td class="lexend-deca d-flex">
                <div class="col-md-3 mx-0 px-0">
                    <img src="${paramPizza.imageUrl}" class="col-md-12">
                </div>
                <div class="col-md-6" >
                    <h4>${paramPizza.name}</h4>
                </div>
            </td>
            <td class="col-1 real-price">${paramPizza.price * paramAmount}</td>
            <td class="col-1">
                <div class="btn-group mb-2" role="group" aria-label="Basic example">
                    <button type="button" class="btn custom-bg-btn btn-subtract" data-id='${paramPizza.id}'>-</button>
                    <button type="button" class="btn custom-bg-btn btn-count-chosen">${paramAmount}</button>
                    <button type="button" class="btn custom-bg-btn btn-plus" data-id='${paramPizza.id}'>+</button>
                </div>
                <small class="text-danger" style="text-decoration: underline;">Remove Item</small>
            </td>
            <td class="col-1">${paramPizza.price}</td>
        </tr>
    `)
}

export default formCart