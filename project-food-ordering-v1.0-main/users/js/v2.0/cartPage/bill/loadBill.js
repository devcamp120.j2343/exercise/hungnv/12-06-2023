import { calculateSubtotal, calculateTotal } from "./countBill.js"
const loadBill = () => {
    $('#sub-total').html(`$${calculateSubtotal()}`)
    $('#total').html(`$${calculateTotal(0)}`)
}
export default loadBill