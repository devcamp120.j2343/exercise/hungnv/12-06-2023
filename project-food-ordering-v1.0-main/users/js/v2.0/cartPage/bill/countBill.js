
//hàm tính subtotal
const calculateSubtotal = () => {
    var vPriceArr = Array.from($('.real-price'))
    return vPriceArr.reduce((ele, cur) => ele += parseFloat(cur.innerText), 0)
}

//hàm tính total = subtotal + shipping fee
const calculateTotal = (paramPercent) => {
    return (calculateSubtotal() + 20) * (100 - paramPercent) / 100
}

export { calculateSubtotal, calculateTotal }