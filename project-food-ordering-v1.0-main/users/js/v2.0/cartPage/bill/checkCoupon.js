import { calculateTotal } from "./countBill.js"
const gCOUPON_URL = 'https://food-ordering-fvo9.onrender.com/api/vouchers/'
var gDiscountPercent;
var gCouponObj;
const checkCoupon = () => {
    var vCoupon = $('#inp-coupon').val().trim()
    if (validateCouponInp(vCoupon)) {
        $.ajax({
            url: `${gCOUPON_URL}?voucherCode=${vCoupon}`,
            type: 'GET',
            async: false,
            data: 'json',
            success: res => {
                handleCoupon(res)
                gCouponObj = res
            },
            error: err => console.log(err)
        })
    } else {
        alert('Hãy nhập mã giảm giá!')
    }
}

const validateCouponInp = (paramCouponInp) => {
    if (paramCouponInp == '') {
        gDiscountPercent = 0
        $('#coupon').html('NO')
        return false
    }
    return true
}

const handleCoupon = (paramCoupon) => {
    if (paramCoupon.length == 1) {
        console.log(paramCoupon)
        gDiscountPercent = paramCoupon[0].discount
        alert(`Thông tin mã giảm giá gồm: ${JSON.stringify(paramCoupon[0])}`)
        $('#coupon').html(`${paramCoupon[0].voucherCode}`)
        $('#discount-percent').html(`${paramCoupon[0].discount}`)
        $('#total').html(`$${calculateTotal(gDiscountPercent)}`)
    } else {
        alert('Mã giảm giá không tồn tại')
        gDiscountPercent = 0
        $('#total').html(`$${calculateTotal(gDiscountPercent)}`)
        $('#discount-percent').html(gDiscountPercent)
        $('#coupon').html('NO')
    }
}
export { checkCoupon, gCouponObj }