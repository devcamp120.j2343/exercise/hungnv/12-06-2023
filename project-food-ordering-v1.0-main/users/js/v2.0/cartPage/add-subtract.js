import loadBill from "./bill/loadBill.js";
import loadCart from "./loadCarts.js";
import { handleStorage, getStorage } from "./storage.js";

var gCartsFiltered
gCartsFiltered = handleStorage(getStorage())

const changeQuantity = (paramBtn) => {
    const vPizzaId = paramBtn.getAttribute('data-id')
    console.log(gCartsFiltered);
    gCartsFiltered.map(arr => {
        if (arr[0].id === vPizzaId) {
            switch ($(paramBtn).html()) {
                case '+':
                    arr[1]++
                    $('#cart-tbody').html('')
                    loadCart()
                    loadBill()
                    break
                case '-':
                    arr[1]--
                    $('#cart-tbody').html('')
                    loadCart()
                    loadBill()
                    break
                default:
                    console.log('error');
            }
        }
    })
}
export { gCartsFiltered }
export default changeQuantity