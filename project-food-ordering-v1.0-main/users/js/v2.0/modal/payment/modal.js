const paymentModal = () => {
    return (`
    <div id="create-payment-modal" class="modal fade" tabindex="-1">
        <div class="modal-dialog modal-lg">
            <div class="modal-content lexend-deca">
                <div class="modal-header">
                    <button class="close mr-4 custom-text-color" style="font-size: 35px;"
                        data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <h1 class="modal-title text-center ABeeZee custom-text-color" style="font-size: 32px;">
                        Make Payment
                    </h1>
                    <div class="text-center mr-4">
                        <img src="./imgs/Stepper-payment-v2.0.png" id="stepper-payment">
                    </div>
                    <div>
                        <div class="row px-5 justify-content-around">
                            <div class="form-group col-md-6">
                                <input type="text" id="inp-firstname" placeholder="Firstname"
                                    class="form-control custom-inp-modal py-4">
                                <input type="text" id="inp-email" placeholder="Email Address"
                                    class="form-control custom-inp-modal py-4">
                                <div>
                                    <label class="custom-text-color">Select Method of Payment</label>
                                    <div class="form-check custom-method-payment">
                                        <input class="form-check-input" type="radio" name="flexRadioDefault"
                                            id="flexRadioDefault1" value='CreditCard'>
                                        <label class="form-check-label" for="flexRadioDefault1">
                                            <img src="./imgs/Group.png">
                                            Credit Card Or Debit
                                        </label>
                                    </div>
                                    <div class="form-check custom-method-payment">
                                        <input class="form-check-input" type="radio" name="flexRadioDefault"
                                            id="flexRadioDefault2" value='Paypal'>
                                        <label class="form-check-label" for="flexRadioDefault2">
                                            <img src="./imgs/Paypal.png">
                                            Paypal
                                        </label>
                                    </div>
                                    <div class="form-check custom-method-payment">
                                        <input class="form-check-input" type="radio" name="flexRadioDefault"
                                            id="flexRadioDefault3" value='BankTransfer'>
                                        <label class="form-check-label" for="flexRadioDefault3">
                                            <img src="./imgs/Bank.png">
                                            Bank Transfer
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <input type="text" id="inp-lastname" placeholder="Lastname"
                                    class="form-control custom-inp-modal py-4">
                                <input type="text" id="inp-address" placeholder="Address for Delivery"
                                    class="form-control custom-inp-modal pt-4"
                                    style="padding-bottom: 90px;">
                                <input type="text" id="inp-phone" placeholder="Mobile Phone"
                                    class="form-control custom-inp-modal py-4">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer d-flex justify-content-center">
                        <button class="btn btn-lg px-5 custom-btn-go-to-payment" id="btn-payment">Go to payment</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    `)
}
export default paymentModal