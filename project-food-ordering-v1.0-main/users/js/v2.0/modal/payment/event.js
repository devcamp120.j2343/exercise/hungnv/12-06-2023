import { gCartsFiltered } from "../../cartPage/add-subtract.js";
import { gCouponObj } from "../../cartPage/bill/checkCoupon.js";
const gORDER_URL = "https://food-ordering-fvo9.onrender.com/api/orders";

//khi nhấn nút => thu thập thông tin callAPI để tạo được 1 đơn hàng mới
const onBtnGoToPayment = () => {
    var vOrderObj = {}
    vOrderObj = getOrder()
    console.log(vOrderObj.details);
    if (validateOrder(vOrderObj)) {
        callAPIToPostOrder(vOrderObj)
    }
}

//B1: hàm thu thập dữ liệu đơn hàng
const getOrder = () => {
    var vRadioCheckedVal = $('input[name=flexRadioDefault]:checked').val()
    return (
        {
            firstName: $('#inp-firstname').val(),
            lastName: $('#inp-lastname').val(),
            email: $('#inp-email').val(),
            address: $('#inp-address').val(),
            phone: $('#inp-phone').val(),
            // Payment method là 1 trong 3 giá trị: CreditCard - Paypal - BankTransfer
            methodPayment: vRadioCheckedVal,
            // Voucher id lấy giá trị id khi kiểm tra mã giảm giá (Xem cấu trúc phần (**))
            voucherId: gCouponObj?.id,//có đc obj từ callAPI nút redeem?
            details: makeDetailsOrder()
        }
    )
}

//B2: hàm validate dữ liệu
const validateOrder = (paramOrder) => {
    if (paramOrder.details.length == 0) {
        alert('Hãy chọn pizza trước khi thanh toán nhé^^')
        return false
    }
    if (paramOrder.firstName == '') {
        alert('Hãy nhập firstname')
        return false
    }
    if (paramOrder.lastName == '') {
        alert('Hãy nhập lastname')
        return false
    }
    if (paramOrder.email == '' || !validateEmail(paramOrder.email)) {
        alert('Hãy nhập email đúng định dạng')
        return false
    }
    if (paramOrder.address == '') {
        alert('Hãy nhập address')
        return false
    }
    if (paramOrder.phone == '' || !validatePhone(paramOrder.phone)) {
        alert('Hãy nhập số điện thoại đúng định dạng')
        return false
    }
    if (paramOrder.methodPayment == undefined) {
        alert('Hãy nhập methodPayment')
        return false
    }
    // còn details,voucherId
    return true
}

//B3: call API để tạo mới order
const callAPIToPostOrder = (paramOrder) => {
    $.ajax({
        url: gORDER_URL,
        type: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        data: JSON.stringify(paramOrder),
        success: res => handleCreateOrder(res),
        error: err => console.log(err)
    })
}

//B4: xử lý khi call api thêm mới đơn hàng thành công
const handleCreateOrder = (paramRes) => {
    console.log(paramRes)
    localStorage.clear()
    $('#create-payment-modal').modal('hide')
    $('#create-success-payment-modal').modal('show')
}

//hàm validate email
export const validateEmail = (paramEmail) => {
    var vRegexEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
    var vCheckedEmail = vRegexEmail.test(paramEmail)
    return vCheckedEmail
}

//hàm validate phone
export const validatePhone = (paramPhone) => {
    var vRegexPhone = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/
    var vCheckedPhone = paramPhone.match(vRegexPhone)
    return vCheckedPhone
}

//hàm tạo details({id món ăn, số lượng của món đó})
const makeDetailsOrder = () => {
    return gCartsFiltered.map(detailArr => {
        console.log(detailArr);
        return (
            {
                foodId: detailArr[0].id,
                quantity: detailArr[1]
            }
        )
    })
}
export default onBtnGoToPayment