import { gPizzaChosen } from "./cartPage/storage.js"
import loadCart from "./cartPage/loadCarts.js"
import { gCartsFiltered } from "./cartPage/add-subtract.js"
import loadBill from "./cartPage/bill/loadBill.js"
import { changeCart } from "./from v1.0 to v2.0/choosePizza.js"
import addModalToHtml from "./modal/addModalToHTML.js"
const onLoadingCartPage = () => {
    //hàm check storage có pizza không để chuyển màu icon giỏ hàng
    changeCart(gPizzaChosen)
    loadCart()
    console.log(gCartsFiltered);
    loadBill()
    addModalToHtml()
}

export default onLoadingCartPage