import deleteModal from "./delete/modal.js"
import { callAPIToLoadOrders } from "./loadTable/callAPI.js"
import updateModal from "./update/modal.js"

const onLoadingPage = () => {
    callAPIToLoadOrders()
    $('#update-modal').append(updateModal())
    $('#delete-modal').append(deleteModal())
}
export default onLoadingPage