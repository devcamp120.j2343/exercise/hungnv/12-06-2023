const updateModal=()=>{
    return(`
    <div id="update-order-modal" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content lexend-deca">
            <div class="modal-header">
                <button class="close mr-4 custom-text-color" style="font-size: 35px;"
                    data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <h1 class="modal-title text-center ABeeZee custom-text-color" style="font-size: 32px;">
                    CHI TIẾT ĐƠN HÀNG - MÃ: <span id="span-order-code">AAA</span>
                </h1>
                <div>
                    <div class="d-flex py-3">
                        <div class="d-flex col-md-6 align-items-center">
                            <label for="inp-lastname" class="col-md-5 ">
                                Họ và đệm
                                <span class="text-danger">(*)</span>
                            </label>
                            <input type="text" class="form-control" id="inp-lastname"
                                placeholder="Họ và tên đệm...">
                        </div>
                        <div class="d-flex col-md-6 align-items-center">
                            <label for="inp-firstname" class="col-md-5 ">
                                Tên
                                <span class="text-danger">(*)</span>
                            </label>
                            <input type="text" class="form-control" id="inp-firstname" placeholder="Tên...">
                        </div>
                    </div>
                    <div class="d-flex py-3">
                        <div class="d-flex col-md-6 align-items-center">
                            <label for="inp-email" class="col-md-5 ">
                                Email
                                <span class="text-danger">(*)</span>
                            </label>
                            <input type="text" class="form-control" id="inp-email" placeholder="Email...">
                        </div>
                        <div class="d-flex col-md-6 align-items-center">
                            <label for="inp-phone" class="col-md-5 ">
                                Phone
                                <span class="text-danger">(*)</span>
                            </label>
                            <input type="text" class="form-control" id="inp-phone"
                                placeholder="Số điện thoại...">
                        </div>
                    </div>
                    <div class="d-flex py-3">
                        <div class="d-flex col-md-12">
                            <label class="pl-3" style="width: 25%;">
                                Address
                                <span class="text-danger">(*)</span>
                            </label>
                            <input type="text" class="form-control" id="inp-address" placeholder="Address...">
                        </div>
                    </div>
                    <div class="d-flex pt-3">
                        <div class="d-flex col-md-6 align-items-center">
                            <label for="select-payment" class="col-md-5 ">
                                Payment Method
                                <span class="text-danger">(*)</span>
                            </label>
                            <select id="select-payment" class="form-control">
                                <option value="0" selected>Chọn phương thức thanh toán</option>
                                <option value="CreditCard">Credit Card</option>
                                <option value="Paypal">Paypal</option>
                                <option value="BankTransfer">Bank Transfer</option>
                            </select>
                        </div>
                        <div class="d-flex col-md-6 align-items-center">
                            <label for="inp-voucher" class="col-md-5 ">
                                Mã giảm giá
                                <span class="text-danger">(*)</span>
                            </label>
                            <input type="text" class="form-control" id="inp-voucher"
                                placeholder="Voucher Code...">
                        </div>
                    </div>
                    <div class="d-flex">
                        <div class="d-flex col-md-12 justify-content-end">
                            <p class="col-md-5 text-center">
                                (Được giảm <span style="font-weight: 500;" id="span-discount">10%</span>)
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer d-flex">
                <button class="btn btn-lg btn-secondary px-5" id="btn-cancel-order" data-dismiss="modal">Hủy</button>
                <button class="btn btn-lg btn-success px-5" id="btn-update-order">Cập nhật</button>
            </div>
        </div>
    </div>
</div>
    `)
}

export default updateModal