import { callAPIToCheckVoucher } from "../loadTable/callAPI.js";
import { validateEmail, validatePhone } from "../../../users/js/v2.0/modal/payment/event.js";
import { gBASE_URL } from "../constants/constants.js";
import { gId } from "./iconEvent.js";
import { fillDetailToModal } from "./iconEvent.js";
var gVoucherArr = callAPIToCheckVoucher()
const updateOrder = () => {
    //B1: thu thập dữ liệu
    var vOrderObj = {}
    vOrderObj = getOrder()
    console.log(vOrderObj);
    //B2: validate dữ liệu
    if (validateOrder(vOrderObj)) {
        //B3: call API để cập nhật đơn hàng
        callAPIToUpdateOrder(vOrderObj, gId)
    }
}

//B1: thu thập dữ liệu
const getOrder = () => {
    var vVoucher = getVoucher($('#inp-voucher').val())
    return (
        {
            id: gId,
            orderCode: $('#span-order-code').html(),
            firstName: $('#inp-firstname').val(),
            lastName: $('#inp-lastname').val(),
            email: $('#inp-email').val(),
            phone: $('#inp-phone').val(),
            address: $('#inp-address').val(),
            methodPayment: $('#select-payment').val(),
            voucherId: vVoucher.id
        }
    )
}

//B2: validate dữ liệu
const validateOrder = (paramOrder) => {
    if (paramOrder.orderCode == '') {
        alert('Hãy nhập mã đơn hàng')
        return false
    }
    if (paramOrder.firstName == '') {
        alert('Hãy nhập tên')
        return false
    }
    if (paramOrder.lastName == '') {
        alert('Hãy nhập họ và tên đệm')
        return false
    }
    if (paramOrder.email == '' || !validateEmail(paramOrder.email)) {
        alert('Hãy nhập email đúng định dạng')
        return false
    }
    if (paramOrder.phone == '' || !validatePhone(paramOrder.phone)) {
        alert('Hãy nhập số điện thoại đúng định dạng')
        return false
    }
    if (paramOrder.address == '') {
        alert('Hãy nhập địa chỉ')
        return false
    }
    if (paramOrder.methodPayment == undefined) {
        alert('Hãy chọn phương thức thanh toán')
        return false
    }
    return true
}

//B3: call API để cập nhật đơn hàng
const callAPIToUpdateOrder = (paramOrder, paramOrderId) => {
    $.ajax({
        url: gBASE_URL + paramOrderId,
        type: 'PUT',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        data: JSON.stringify(paramOrder),
        success: res => handleResponse(res),
        error: err => console.log(err)
    })
}

//B4: xử lý hiển thị sau khi put thành công
const handleResponse = (paramRes) => {
    var vConfirm = confirm('Đã cập nhật thành công đơn hàng')
    if (vConfirm) {
        fillDetailToModal('')
        $('#update-order-modal').modal('hide')
        location.reload()
    }
}

//B1.a: get voucher id từ voucher code trong input
const getVoucher = (paramVoucherCode) => {
    var vVoucher = {}
    if (paramVoucherCode != '') {
        var vVoucherFound = gVoucherArr.find(voucher => voucher.voucherCode === paramVoucherCode)
        if (vVoucherFound != undefined) {
            vVoucher = vVoucherFound
        } else {
            alert('Mã voucher không tồn tại!')
            vVoucher = {}
        }
    } else {
        vVoucher = {}
    }
    return vVoucher
}

export default updateOrder