import getDetailFromIcon from "../loadTable/getDetailIcon.js"
import { callAPIToCheckVoucher } from "../loadTable/callAPI.js";

var gId;
const onEditIconClick = (paramIcon) => {
    var vDetail = getDetailFromIcon(paramIcon)
    gId = vDetail.id
    console.log('Id của đơn hàng đã chọn là: ' + vDetail.id);
    //truyền dữ liệu vào modal
    fillDetailToModal(vDetail)
}

//hàm truyền dữ liệu vào modal
export const fillDetailToModal = (paramDetail) => {
    var vVoucher = checkVoucher(paramDetail.voucherId)
    $('#span-order-code').html(paramDetail.orderCode)
    $('#inp-lastname').val(paramDetail.lastName)
    $('#inp-firstname').val(paramDetail.firstName)
    $('#inp-email').val(paramDetail.email)
    $('#inp-phone').val(paramDetail.phone)
    $('#inp-address').val(paramDetail.address)
    $('#select-payment').val(paramDetail.methodPayment)
    $('#inp-voucher').val(vVoucher.voucherCode)
    $('#span-discount').html(vVoucher.discount + '%')
}

const checkVoucher = (paramVoucherId) => {
    var vVoucherArr = callAPIToCheckVoucher()
    if (paramVoucherId != null) {
        var vCorrectVoucher = vVoucherArr.filter(voucher => voucher.id === paramVoucherId)[0]
        return vCorrectVoucher//{}
    } else {
        return (
            {
                voucherCode: '',
                discount: 0
            }
        )
    }
}

export { onEditIconClick, gId }