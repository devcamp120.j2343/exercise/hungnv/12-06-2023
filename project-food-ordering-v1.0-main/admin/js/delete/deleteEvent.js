import { gBASE_URL } from "../constants/constants.js";
import { gId } from "../update/iconEvent.js";

const deleteOrder = () => {
    $.ajax({
        url: gBASE_URL + gId,
        type: 'DELETE',
        success: handleDelete,
        error: err => console.log(err)
    })
}
const handleDelete = () => {
    alert('Đã xóa thành công đơn hàng')
    location.reload()
    $('#delete-order-modal').modal('hide')
}
export default deleteOrder