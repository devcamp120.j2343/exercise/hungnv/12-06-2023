var gSTT = 0
export const gTable = $('#table-orders').DataTable({
    columns: [
        { data: 'id' },
        { data: 'orderCode' },
        { data: { firstName: 'firstName', lastName: 'lastName' } },
        { data: 'methodPayment' },
        { data: 'foods' },
        { data: 'foods' },//tổng tiền
        { data: 'createdAt' },
        { data: 'action' }
    ],
    columnDefs: [
        {
            targets: 0,
            render: renderSTT
        },
        {
            targets: 2,
            render: renderFullName
        },
        {
            targets: 4,
            render: renderQuantity
        },
        {
            targets: 5,
            render: renderTotal
        },
        {
            targets: 7,
            defaultContent: `
            <img class="edit-order" src="https://cdn0.iconfinder.com/data/icons/glyphpack/45/edit-alt-512.png" style="width: 20px;cursor:pointer;">
            <img class="delete-order" src="https://cdn4.iconfinder.com/data/icons/complete-common-version-6-4/1024/trash-512.png" style="width: 20px;cursor:pointer;">
            `
        }
    ]
})

function renderSTT() {
    gSTT++
    return gSTT
}

function renderFullName(paramArr) {
    console.log(paramArr);
    return `${paramArr.firstName} ${paramArr.lastName}`
}
function renderQuantity(paramFoodsArr) {
    return paramFoodsArr.length
}

function renderTotal(paramFoodsArr) {
    var vTotal = paramFoodsArr.reduce((acc, cur) => acc += cur.price, 0)
    return vTotal
}

function drawTable(paramOrders) {
    gTable.clear()
    gTable.rows.add(paramOrders)
    gTable.draw()
}
export default drawTable