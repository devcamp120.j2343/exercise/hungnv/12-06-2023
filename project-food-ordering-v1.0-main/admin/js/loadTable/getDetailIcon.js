import { gTable } from "./drawTable.js"
const getDetailFromIcon = (paramIcon) => {
    var vRow = $(paramIcon).parents('tr')
    var vDetail = gTable.row(vRow).data()
    return vDetail
}
export default getDetailFromIcon