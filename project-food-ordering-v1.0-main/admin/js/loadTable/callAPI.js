import { gBASE_URL, gVOUCHER_URL } from "../constants/constants.js"
import drawTable from "./drawTable.js"

const callAPIToLoadOrders = () => {
    $.ajax({
        url: gBASE_URL,
        type: 'GET',
        success: res => { console.log(res), drawTable(res) },
        error: err => console.log(err)
    })
}

const callAPIToCheckVoucher = () => {
    var vVoucherArr = []
    $.ajax({
        url: gVOUCHER_URL,
        type: 'GET',
        async: false,
        success: res => vVoucherArr = res,
        error: err => console.log(err)
    })
    return vVoucherArr
}

export { callAPIToLoadOrders, callAPIToCheckVoucher }