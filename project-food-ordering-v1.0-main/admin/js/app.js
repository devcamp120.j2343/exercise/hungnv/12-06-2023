import deleteOrder from "./delete/deleteEvent.js"
import { onDeleteIconClick } from "./delete/iconEvent.js"
import onLoadingPage from "./loadPage.js"
import { onEditIconClick } from "./update/iconEvent.js"
import updateOrder from "./update/updateEvent.js"

const app = () => {
    onLoadingPage()

    //gán sự kiện nút bút chì(update đơn hàng)
    $('#tbody-orders').on('click', '.edit-order', function () {
        onEditIconClick(this)
        $('#update-order-modal').modal('show')
    })
    //gán sự kiện update order trên modal
    $('#btn-update-order').on('click', function () {
        updateOrder()
    })

    //gán sự kiện nút thùng rác(xóa đơn hàng)
    $('#tbody-orders').on('click', '.delete-order', function () {
        onDeleteIconClick(this)
        $('#delete-order-modal').modal('show')
    })
    //gán sự kiện delete order trên modal
    $('#btn-delete-order').on('click', function () {
        deleteOrder()
    })

}
app()